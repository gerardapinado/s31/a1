const Task = require('../models/Task')

module.exports.createTask = async (reqBody) => {
    const {name} = reqBody
    let newTask = new Task({
        name: name
    })

    return await newTask.save().then(result => result ? true : false)
}

module.exports.getTask = async (id) => {
    return await Task.findById(id).then(result => result)
}

module.exports.getAllTask = async () => {
    return await Task.find().then(result => result)
}

module.exports.updateTask = async (id) => {
    return await Task.findByIdAndUpdate(id, {$set: {status:"complete"}}, {new:true}).then((result,err) => result ? result : err)
}