const express = require('express')
const router = express.Router()

const taskController = require('../controllers/taskController')

router.post('/createTask', async(req,res) => {
    try{
        await taskController.createTask(req.body).then(result => res.status(201).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.get('/:id', async(req,res) => {
    try{
        await taskController.getTask(req.params.id).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.get('/', async(req,res) => {
    try{
        await taskController.getAllTask().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.put('/:id/complete', async(req,res) => {
    try{
        await taskController.updateTask(req.params.id).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

module.exports = router

