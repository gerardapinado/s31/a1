const mongoose = require('mongoose')
const express = require('express')
const dotenv = require('dotenv').config()

const taskRouter = require('./routes/taskRoutes')

const PORT = 3000
const app = express()

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))

app.use(`/api/tasks`, taskRouter)

app.listen(PORT, () => console.log(`Connected to ${PORT}`))